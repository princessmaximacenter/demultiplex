# demultiplex

Code for demultiplexing fastq files, and a library for flexibly allowing mismatches during barcode recognition.

This code used to be maintained at https://github.com/plijnzaad/demultiplex

## Contents

 * demultiplex.pl - the actual demultiplexer. Input is read from stdin, output is written to `*.fastq.gz`
 * edit-distance.pl - for spotting pairs of barcodes that are too close  in terms of Hamming distance
 * edit-distance-matrix.pl - print all the pairwise Hamming distance (to help choose or optimize barcodes)
 * mismatch.pm - library that deals with the mismatching
 * testdata - test data
 * hyperplex.R - code to help create groups of barcodes (if they are deficient)

## Mismatching

If the barcodes that were used are distinct enough, you can recover some
of the barcodes that are lost due to sequencing error (the default is to
allow 1 mismatch).  This is safe as long as the minimum edit distance
amongst all the barcodes is 3 or higher. If most of the distances are >=
2 but some are only 1, you can still allow most mismatches in these
barcodes, apart from the positions inside the barcodes where these pairs
of barcodes do not differ enough. This is done by specifying these
positions using *lowercase* letters. In contrast to the uppercase
letters, these are never allowed to mismatch. (Any lowercase letters in
the barcode file is of course uppercased before the matching; the
lowercase is only used for conveniently specifying the fixed position).

Note that if _k_ mistmatches are to be allowed, the minimum edit
distance among all barcodes must be at least _k+2_ (since otherwise the
mismatched code is exactly halfway between two exact barcodes).

The script `check-barcodes.pl` can be used to check for clashes between
barcodes with mismatches, and to improve faulty barcodes. 

When creating mixes of barcodes (e.g. to multiply the number of UMI's
that are available), you may be able to ignore the fixed positions (and
allow one mismatch on every position), but only if you choose the mixes
properly. For this, the minimum Hamming distance among any pair of
barcode mixes of course must also be >= 3. For this, the hyperplex.R
script is provided.

## Using the code

To start using it, do 

```
$ git clone https://bitbucket.org/princessmaximacenter/demultiplex.git
```

This will create a directory `demultiplex`. To check an existing set of
barcodes (or when combining sets of barcodes), do:

```
$ ~/git/demultiplex/check-barcodes.pl -m 1 testdata/mm1-ambiguous-codes.txt  # 10 ambiguous
$ ~/git/demultiplex/check-barcodes.pl -m 1 testdata/testbarcodes.txt # will  # none
```

Add ` -o exactpos-barcodes.txt ` to the above lines to calculate barcodes
with fixed positions, i.e. those that are not allowed to be
mismatched. Such a barcode file can be used to demultiplex with
potentially faulty barcodes as:

```
$ mkdir testoutput
$ ./demultiplex-fastq.pl -m 1 -b testdata/testbarcodes.txt -o testoutput  < testdata/nomismatches.fastq 
```

To get a usage message on a script, run the script with an `-h` flag.

I'm interested in feedback and improvements, please report issues and/or fork it and send me pull requests.

Philip Lijnzaad <plijnzaad@gmai.com>
