#!/usr/bin/env perl
use strict;
use Getopt::Std;

## make sure the script can find the mismatch.pm library, which also in the current dir:
use File::Basename;
use lib dirname(__FILE__);
use mismatch;
## use Text::Levenshtein qw(distance);     

## written by plijnzaad@gmail.com

use vars qw($opt_h $opt_m $opt_g $opt_b);

my $Usage="Find near-duplicates among barcodes. 

For list of strings on stdin (format: name TAB barcode), print all the
sets of strings with distance less than or equal NUMBER to each other.
Mismatched letters are 'highlighted' in lowercase.

Usage: 
  $0 -m NUMBER  [ -g ]  [ < ] barcodefile.txt > output

Options:

 -g  Alternative output: barcode IDs as pairs of nodes forming a graph edge 
     (used by hyperplex.R)
 
 -b  As -g  but outputting the barcodes rather than their IDs

Note that the minimum distance needed for unambiguous demultiplexing allowing 1 mismatch is
*3*, not 2. So typically you invoke the script with -m 2 .

";

if ( !getopts("hm:gb") || $opt_h ) {
    die $Usage; 
}

die "-m option missing " unless defined($opt_m);

my $limit= $opt_m || 2;

my  $strings={};

### read codes
LINE:
while(<>) { 
  s/#.*//;
  s/[ \t\n\r]*$//g;
  next LINE unless $_;
  my ($name, $code)=split(' ');            # e.g. 'G7 \t CCAACAAT'
  if (! $code) {                           # just one column of strings
    $code=$name;
    $name= "(line $.)";
  }
  die "duplicate string: '$code' (named '$name', line $.,)" if $strings->{$code};
  $strings->{$code}=$name;
}                                       # while

my @strings = sort keys %$strings;

my $show = ($opt_g || $opt_b) ? 'pairs' : 'sets';

my $ncases=0;

print "# $show of barcodes with edit distance <= $limit:\n";
SEQ:
for(my $i=0; $i<@strings; $i++) { 
  my @s=@strings;
  my $s=$s[$i];
  splice(@s, $i, 1);
  my @d = map { mismatch::hammingdist($_, $s); } @s;
  my @hits = grep( $d[$_] <= $limit, 0..$#d);
  next SEQ unless @hits;
  
  $ncases++;
  if( $show eq 'pairs' ) {
    for(my $h=0; $h<@hits; $h++) {
      my $hs=$s[ $hits[$h] ];
      if ($opt_b) {
        print "$s\t$hs\n";
      } else {
        print "$strings->{$s}\t$strings->{$hs}\n";
      }
    }
    print . "\t$s\n";     
  } else {
    print $strings->{$s} . "\t$s\n"; 
    for(my $h=0; $h<@hits; $h++) {
      my $hs=$s[ $hits[$h] ];
      my $mm=mismatch::format_mm($s, $hs);
      print "$strings->{$hs}\t$mm (d=$d[ $hits[$h] ])\n";
    }
    print "\n";
  } 
}
warn "Found $ncases clusters of clashing barcodes\n";
