#!/usr/bin/env perl
# See usage

use strict;
use Getopt::Std;

use mismatch;

use vars qw($opt_f $opt_h);

my $version=mismatch::getversion($0);
my @args=@ARGV;

my $Usage="Typical usage:

  for i in 0 1 2 3 4 5 6 7 8 ; do 
    $0 -f \$i barcodes.txt > codes-fixed\$i.txt 2>codes-fixed\$i.log
  done 

Given a barcodefile (format: id \\t barcode \\n) with lowercase characters
indicating positions that cannot be mismatched (as e.g. output by
check-barcodes.pl), output the barcodes by number of fixed positions (and
clearly, the fewer the better).
";

if ( !getopts("f:h") || $opt_h  || @ARGV  != 1 ) {
    die $Usage; 
}

my $barcodefile=$ARGV[0];

die "-f option missing " unless defined($opt_f);

warn "Running $0, version $version, with args @args\n";

my $barcodes_mixedcase = mismatch::readbarcodes($barcodefile); ## eg. $h->{'AGCGtT') => 'M3'

my $ncases=0;
print "## barcodes having $opt_f fixed positions:\n";
foreach my $code ( keys %$barcodes_mixedcase ) {
  my $n=mismatch::countlowercase($code);
  if ($n == $opt_f) { 
    my $id=$barcodes_mixedcase->{$code};
    print "$id\t$code\n";
    $ncases++;
  }
}

warn "Found $ncases barcodes having $opt_f lowercase letters\n";

1;
