#!/usr/bin/env perl
# See usage

use strict;
use Getopt::Std;
use FileHandle;
use Algorithm::Combinatorics;
use List::Util qw(min);

## make sure the script can find the mismatch.pm library, which also in the current dir:
use File::Basename;
use lib dirname(__FILE__);
use mismatch;

use vars qw($opt_h $opt_m $opt_o);

my $version=mismatch::getversion($0);
my @args=@ARGV;


my $Usage="Usage:

   $0  -m M  [ -o exactpos-barcodes.txt ]   barcodes.txt

Given a barcodefile (format: id \\t barcode \\n), find all the barcodes
that have ambiguous potential barcode misreads.  The output (to stdout) is 

MISREAD : BARCODE1 BARCODE2 [ BARCODE3 etc.]  

Here, MISREAD is any barcode with M sequencing errors, BARCODE1 etc. are the
real barcodes to which it corresponds, with mismatching positions
indicated in lowercase. Statistics are written to stderr.

When the -o FILE option is used, it will calculate new barcodes that are
written to FILE. These new barcodes are unambiguous while allowing for
mismatches. These barcodes have lowercase letters in places where
mismatches cannot be tolerated.  This output can be used by the
demultiplex-{sam,fastq}.pl scripts (they use the same convention:
mismatches are never allowed on lowercase letters).  Note that running the
current script on this new file will therefore not find any ambiguity
anymore. For a testcase, see the file testdata/mm1-ambiguous-codes.txt

Written by <plijnzaad\@gmail.com>
";

if ( !getopts("m:o:h") || $opt_h  || @ARGV!= 1 ) {
    die $Usage; 
}

die "-m option missing " unless defined($opt_m);

warn "Running $0, version $version, with args @args\n";

my  $allowed_mismatches = $opt_m;

my $barcodefile=$ARGV[0];

my $barcodes_mixedcase = mismatch::readbarcodes($barcodefile); ## eg. $h->{'AGCGtT') => 'M3'

my $barcodes = mismatch::mixedcase2upper($barcodes_mixedcase);     ## e.g. $h->{'AGCGTT') => 'M3'
my $mismatch_REs = mismatch::convert2mismatchREs(barcodes=>$barcodes_mixedcase, 
                                                 allowed_mismatches =>$allowed_mismatches);# eg. $h->{'AGCGTT') =>  REGEXP(0x25a7788)
my @allbarcodes = keys %$barcodes;

# $barcodes_mixedcase=undef;
my $id2code={};
## invert the table for lookup later on
for my $code (keys %$barcodes_mixedcase) { 
  $id2code->{  $barcodes_mixedcase->{$code} }=$code;
}

my $len=length( (keys %$barcodes)[0]  );
my $n = 4 ** $len ;
warn "Will now check all possible 4 ^ $len i.e. " . $n
    . " barcodes for ambiguous misreads, allowing for $allowed_mismatches mismatches\n";

my $iter= Algorithm::Combinatorics::tuples_with_repetition([ qw(A C G T) ] , $len);

warn "... found the $n barcodes...\n ";

my $nunknown=0;
my $nexact=0;
my $nunique=0;
my $nambiguous=0;
my $badcodes={};

sub merge_codes { 
  ## do something like qw(AcGTT ACgTT ACgTt ) => "AcgTt"
  my (@mm)=@_;

  return $mm[0]  if(@mm==1);

  my @w= map { [ split('', $_) ] } @mm;

  my $len=int(@{$w[0]});

  my @final=();

  for(my $i=0; $i<$len; $i++) { 
    my $h={};
    my @col=map { $_->[$i];} @w ;
    for my $col  ( @col ) { $h->{$col}++; }
    my $n= int(keys %$h);
    push(@final,  ($n==1) ? $col[0] : "\L$col[0]");
  }
  join("", @final);
}

sub hamming {
  ## very fast Hamming distance trick from http://www.perlmonks.org/?node_id=500235
  length( $_[ 0 ] ) - ( ( $_[ 0 ] ^ $_[ 1 ] ) =~ tr[\0][\0] )
}

my $i = 1;
print "# misreads that map ambiguously to mismatched barcodes with $allowed_mismatches mismatches:\n";
WORD:
while(my $inst=$iter->next()) {
      $i++;
  my $w=join("",@$inst);                # potential mismatched
  
  warn sprintf("working on word %d (%.1f%%)\n", $i, $i/$n*100 )  if  $i % 10000 == 1;
  if (exists( $barcodes->{$w})) { 
    $nexact++;
    next WORD;
  }

  # speed-up:
  my $mindist = min( map {  hamming($w, $_); } @allbarcodes);
  if($mindist > $allowed_mismatches) {
    $nunknown++;
    next WORD;
  }
  
  my @codes=mismatch::safe_rescue($w, $mismatch_REs);

  $nunknown +=  (@codes==0);
  $nunique +=  (@codes==1);
  $nambiguous +=  (@codes>1);
  
  if (@codes>1) {               # ambiguous
    my @mm = map { mismatch::format_mm($w, $_) . " ($barcodes->{$_})" ; } @codes;
    print "$w: ". join(' ', @mm) . "\n";
    
    for my $code (@codes) { 
      my $mm=mismatch::format_mm($w, $code);
      $badcodes->{$code}{$mm}++;
    }
  }
}                                       # WORD

if ($opt_o) { 
  open(FILE, "> $opt_o") || die "$opt_o: $!";

  for my $id (sort keys %$id2code ) { 
    my $code=$id2code->{$id};           # mixed case!
    if(exists ($badcodes->{$code}))  {
      my @mm=keys %{$badcodes->{$code}};
      my $mmcode=merge_codes(@mm);
      my $n= mismatch::count_lowercase($mmcode);
      print FILE "$id\t$mmcode\t # $n fixed positions\n";
    } else {
      print FILE "$id\t$code\n";
    }
  }
  close(FILE);
}

warn "\nstats:\n";
warn sprintf("Out of all $n possible $len-mers, there are:\n");
warn sprintf("$nexact (%.1f%%) exact matches (== number of barcodes)\n", 100*$nexact/$n);
warn sprintf("When allowing  $allowed_mismatches mismatches, there are:\n");
warn sprintf("$nunique (%.1f%%) usable (i.e. salvageable and unique) misreads\n", 100*$nunique/$n);
warn sprintf("$nambiguous (%.1f%%) ambiguous (i.e. unsalvageable) misreads\n", 100*$nambiguous/$n);
warn sprintf("$nunknown (%.1f%%) unsalvageable misreads\n", 100*$nunknown/$n);
warn "done\n";

